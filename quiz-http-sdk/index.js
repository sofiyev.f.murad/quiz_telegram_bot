const http = require("./http");

function getTopics({ page = 1 }) {
    return http.get("/topics", {
        params: {
            page
        }
    }).then(function (response) {
        return response
    }).catch(function (error) {
        console.log(error);
    })
}

function getSubTopics({ topicId }) {
    return http.get(`/topics/${topicId}/sub-topics`).then(function (response) {
        return response
    }).catch(function (error) {
        console.log(error);
    })
}

function getQuestions({ subTopicId }) {
    return http.get(`/sub-topics/${subTopicId}`).then(function (response) {
        return response
    }).catch(function (error) {
        console.log(error);
    })
}

module.exports = {
    getTopics,
    getSubTopics,
    getQuestions
}