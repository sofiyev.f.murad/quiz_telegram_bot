module.exports = function responseHandler(response) {
    const { data: responseBody } = response;
    const { data } = responseBody;
    return data;
}