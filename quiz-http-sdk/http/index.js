const axios = require("axios");

const responseHandler = require("./responseHandler");
const errorHandler = require("./errorHandler");

const axiosInstance = axios.create({
    baseURL: 'http://wdaddy.bigdata.az/api/',
});

axiosInstance.interceptors.response.use(responseHandler, errorHandler)

module.exports = axiosInstance;