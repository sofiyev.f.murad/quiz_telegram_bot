function getTopicFromMessage(topics, message) {
    return topics.find((topic) => {
        const { topic_name } = topic;
        return topic_name.toLowerCase() === message;
    });
}

function getSubTopicFromMessage(subTopics, message) {
    return subTopics.find((subTopic) => {
        const { id } = subTopic;
        return id === (message << 0);
    });
}

function shuffleAnswer(string) {
    let shuffle = [...string];
    const getRandomValue = (i, N) => Math.floor(Math.random() * (N - i) + i);
    shuffle.forEach((elem, i, arr, j = getRandomValue(i, arr.length)) => [arr[i], arr[j]] = [arr[j], arr[i]]);
    shuffle = shuffle.join('');
    return shuffle;
}

module.exports = {
    getTopicFromMessage,
    getSubTopicFromMessage,
    shuffleAnswer
}