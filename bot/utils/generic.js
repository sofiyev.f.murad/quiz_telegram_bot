function removeSpaceBetweenWords(string) {
    return string.replace(/ +(?= )/g,'');
}

function removeSpaceFromString(string) {
    return string.replace(/\s/g, '');
}

function removeSlashFromString(string) {
    return string.replace(/\//g, '');
}

function chunkArray(myArray, chunk_size) {
    var results = [];

    while (myArray.length) {
        results.push(myArray.splice(0, chunk_size));
    }

    return results;
}

module.exports = {
    removeSpaceBetweenWords,
    removeSlashFromString,
    chunkArray
}