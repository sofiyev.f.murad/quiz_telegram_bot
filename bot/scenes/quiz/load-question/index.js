const quizHTTPSDK = require("../../../../quiz-http-sdk");
const loadSubTopics = require("../../quiz/load-sub-topics");

const { loadQuestionResponse, loadQuestionResponseWithScore } = require("../../../locals/az/load-question");
const { getSubTopicFromMessage } = require("../../../utils/quiz");

module.exports = async function loadQuestion(ctx) {
    const {
        session: {
            quiz: {
                subTopics,
                questions,
                currentQuestion
            },
            user
        },
        callbackQuery
    } = ctx;



    if(ctx.session.flag) {
        if(currentQuestion.index+1 >= questions.data.length) {
            ctx.session.flag = true;
            loadSubTopics(ctx);
        }else {
            currentQuestion.index = currentQuestion.index+1;
            currentQuestion.question = questions.data[currentQuestion.index].question;
            currentQuestion.answer = questions.data[currentQuestion.index].answer;
            loadQuestionResponseWithScore(ctx, currentQuestion, user.score);
        }
        ctx.session.flag = false;
        return
    }

    const subTopicId = callbackQuery.data;
    const subTopic = getSubTopicFromMessage(subTopics.data, subTopicId);

    if (subTopic) {
        const questionResponse = await quizHTTPSDK.getQuestions({ subTopicId });
        questions.data = questionResponse.data;
        currentQuestion.index = 0;
        currentQuestion.question = questions.data[0].question;
        currentQuestion.answer = questions.data[0].answer;
        loadQuestionResponse(ctx, currentQuestion);
    }


}