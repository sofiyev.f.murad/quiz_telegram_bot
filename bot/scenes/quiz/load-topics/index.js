const quizHTTPSDK = require("../../../../quiz-http-sdk");
const { chunkArray } = require("../../../utils/generic");
const { loadTopicsResponse } = require("../../../locals/az/load-topics");

module.exports = async function loadTopics(ctx) {
    const {
        session: {
            quiz: {
                topics
            }
        }
    } = ctx;


    let topicsRequestParams = {}

    if(topics.pagination.next) {
        topicsRequestParams.page = (topics.pagination.current >> 0) + 1;
    }else {
        topics.data = [];
    }

    const topicsResponse = await quizHTTPSDK.getTopics(topicsRequestParams);


    topics.data = [...topics.data, ...topicsResponse.data];
    topics.pagination.next = topicsResponse.next_page_url;
    topics.pagination.current = topicsResponse.current_page;


    const topicsMediaGroup = topicsResponse.data.map((topic) => {
        let {
            image_name: topicPhoto,
            topic_name: topicName,
        } = topic;

        topicName = `/${topicName.toLowerCase()}`;

        return {
            media: topicPhoto,
            caption: topicName,
            type: 'photo'
        }
    });

    const chunkedTopicsMediaGroup = chunkArray(topicsMediaGroup, 5);
    loadTopicsResponse(ctx, chunkedTopicsMediaGroup, topics.pagination);
}