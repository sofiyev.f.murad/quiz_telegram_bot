const quizHTTPSDK = require("../../../../quiz-http-sdk");

const { removeSlashFromString } = require("../../../utils/generic");
const { getTopicFromMessage } = require("../../../utils/quiz");

const { loadSubTopicsResponse, loadSubTopicsResponseWithScore } = require("../../../locals/az/load-sub-topics");


module.exports = async function loadSubTopics(ctx) {
    const {
        session: {
            quiz: {
                topics,
                subTopics
            },
            user
        },
        message
    } = ctx;


    if(ctx.session.flag) {
        loadSubTopicsResponseWithScore(ctx, subTopics.data, user.score);
        return ;
    }

    const messageTextNormalized = removeSlashFromString(message.text.toLowerCase());

    const topic = getTopicFromMessage(topics.data, messageTextNormalized)

    if(topic) {
        const subTopicsResponse = await quizHTTPSDK.getSubTopics({ topicId: topic.id });
        subTopics.data = subTopicsResponse.data;
        subTopics.pagination.next = subTopicsResponse.next_page_url;
        subTopics.pagination.current = subTopicsResponse.current_page;
        loadSubTopicsResponse(ctx, subTopics.data)
    }
}