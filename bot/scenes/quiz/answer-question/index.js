const { wrongAnswerResponse } = require("../../../locals/az/answer-question");
const { removeSpaceBetweenWords } = require("../../../utils/generic");
const loadQuestion = require("../load-question");

module.exports = async function answerQuestion(ctx) {
    const {
        session: {
            quiz: {
                currentQuestion
            },
            user
        },
        message
    } = ctx;

    const messageTextNormalized = removeSpaceBetweenWords(message.text.toLowerCase());
    if(removeSpaceBetweenWords(currentQuestion.answer.toLowerCase()) === messageTextNormalized) {
        user.score = ++user.score
        ctx.session.flag = true;
        loadQuestion(ctx);
    }else {
        wrongAnswerResponse(ctx);
    }
}