const Scene = require('telegraf/scenes/base');

const enterHandler = require("./enter");
const loadTopicsHandler = require("./load-topics");
const loadSubTopicsHandler = require("./load-sub-topics");
const loadQuestionHandler = require("./load-question");
const answerQuestionHandler = require("./answer-question");

const quizScene = new Scene('quiz');

quizScene.enter(enterHandler);

quizScene.action("load-topics", loadTopicsHandler);

quizScene.on("message", (ctx) => {
    if(ctx.message.text.includes("/")) {
        loadSubTopicsHandler(ctx);
    } else {
        answerQuestionHandler(ctx);
    }
});

quizScene.on('callback_query', loadQuestionHandler);

module.exports = quizScene