const { quizEnterResponse } = require("../../../locals/az/quiz-enter");

module.exports = async function enterHandler(ctx) {
    ctx.session.quiz = {
        topics: {
            pagination: {},
            data: []
        },
        subTopics: {
            pagination: {},
            data: null
        },
        questions: {
            pagination: {},
            data: null
        },
        currentQuestion: {
            index: null,
            answer: null
        }
    }
    ctx.session.user = {
        score: 0
    }
    quizEnterResponse(ctx);
}