const { startResponse } = require("../../../locals/az/start");

module.exports = async function enterHandler(ctx) {
    startResponse(ctx);
}