const Scene = require('telegraf/scenes/base');

const enterHandler = require("./enter");
const startQuizHandler = require("./start-quiz");

const menuScene = new Scene('menu');

menuScene.enter(enterHandler);
menuScene.action("start-quiz", startQuizHandler);

module.exports = menuScene