const { removeSpaceBetweenWords } = require("../../utils/generic");

function botInitializeErrorResponse(ctx) {

    const message = `⚠️<b>Gözlənilməyən bir xəta baş verdi</b>⚠️ \n
        <i>Xaiş edirik bir az sonra bir daha yoxlayın</i>`;

    const normalizedMessage = removeSpaceBetweenWords(message);

    ctx.replyWithHTML(normalizedMessage);
}

module.exports = {
    botInitializeErrorResponse
}