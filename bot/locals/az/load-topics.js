const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');

async function loadTopicsResponse(ctx, chunkedTopicsMediaGroup, pagination) {
    for (const chunkArrayItem of chunkedTopicsMediaGroup) {
        await ctx.replyWithMediaGroup(chunkArrayItem)
    }
    if(pagination.next) {
        ctx.reply(
            `Digər topic-lərə bax`,
            Extra.HTML().markup((m) =>
                Markup.inlineKeyboard([
                    m.callbackButton(`Digər`, "load-topics"),
                ]),
            )
        );
    }
}

module.exports = {
    loadTopicsResponse
}