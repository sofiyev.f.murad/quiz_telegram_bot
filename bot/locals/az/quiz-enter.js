const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');

function quizEnterResponse(ctx) {
    ctx.reply(
        `Ilk öncə kateqroiya seçməlisiniz`,
        Extra.HTML().markup((m) =>
            Markup.inlineKeyboard([
                m.callbackButton(`Bütün kateqoriyalara bax`, "load-topics"),
            ]),
        )
    );
}

module.exports = {
    quizEnterResponse
}