const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');

const { removeSpaceBetweenWords } = require("../../utils/generic");

function startResponse(ctx) {
    ctx.reply(
        removeSpaceBetweenWords(`
              <b>Salam <i>Word Daddy</i> oyununa xoş gəlmisiniz</b>\n
              <i>Oyuna başlamaq üçün başla düyməsini sıxın və ya /quiz əmrini göndərin</i>
        `),
        Extra.HTML().markup((m) =>
            Markup.inlineKeyboard([
                m.callbackButton(`* Başla *`, "start-quiz"),
            ]),
        )
    );
}

module.exports = {
    startResponse
}