const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');

const {removeSpaceBetweenWords} = require("../../utils/generic");

async function loadSubTopicsResponse(ctx, subTopics) {
    ctx.reply(
        `Aşağıdakı səviyyələrdən birini seçin`,
        Extra.HTML().markup((m) =>
            Markup.inlineKeyboard(subTopics.slice().map((subTopic) => {
                return m.callbackButton(`${subTopic.sub_name}`, subTopic.id);
            }))
        )
    );
}

async function loadSubTopicsResponseWithScore(ctx, subTopics, score) {
    ctx.replyWithHTML(
        removeSpaceBetweenWords(`
        <b> Təbriklər siz bu səviyə üzrə bütün sualları cavabladınız </b> 
        \n Topladığınız xal - <b>${score}</b>
        \n <b>Dəvam etmək üçün yeni səviyyə seçin</b>`),
        Extra.HTML().markup((m) =>
            Markup.inlineKeyboard(subTopics.slice().map((subTopic) => {
                return m.callbackButton(`${subTopic.sub_name}`, subTopic.id);
            }))
        )
    );
}


module.exports = {
    loadSubTopicsResponse,
    loadSubTopicsResponseWithScore
}