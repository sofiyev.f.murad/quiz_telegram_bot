const {shuffleAnswer} = require("../../utils/quiz");
const { removeSpaceBetweenWords } = require("../../utils/generic");
async function loadQuestionResponse(ctx, currentQuestion) {
    ctx.replyWithHTML(`Sual - <b> ${currentQuestion.question}</b>\nCavab - <b> ${shuffleAnswer(currentQuestion.answer)} </b>`)
}

async function loadQuestionResponseWithScore(ctx, currentQuestion, score) {
    ctx.replyWithHTML(removeSpaceBetweenWords(`
    <b> Doğru cavab </b> 
    \n Topladığınız xal - <b>${score}</b>
    \n Növbəti Sual - <b>${currentQuestion.question}
    \n </b>Cavab - <b> ${shuffleAnswer(currentQuestion.answer)} </b>`))
}

module.exports = {
    loadQuestionResponse,
    loadQuestionResponseWithScore
}