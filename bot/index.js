require('dotenv').config();

const { Telegraf } = require('telegraf');
const Stage = require('telegraf/stage')
const session = require('telegraf/session')

const menuScene = require("./scenes/menu");
const quizScene = require("./scenes/quiz");

const { botInitializeErrorResponse } = require("./locals/az/error");

const bot = new Telegraf(process.env.TELEGRAM_BOT_TOKEN);

const stage = new Stage([menuScene, quizScene]);

bot.use(session());
bot.use(stage.middleware())

bot.command("start", (ctx) => ctx.scene.enter('menu'))

bot.startPolling().catch((error, ctx) => {
    console.log(error.message);
    botInitializeErrorResponse(ctx);
});
